<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Committees extends CI_Controller {
    public function arf()
	{
        $this->load->helper('url');
        $this->load->view('header');
        $data['currenturl'] = $this->uri->uri_string();
        $this->load->view('navbar', $data);
		    $this->load->view('council');
        $this->load->view('arf');
        $this->load->view('footer');
	}

    public function unca()
	{
        $this->load->helper('url');
        $this->load->view('header');
        $data['currenturl'] = $this->uri->uri_string();
        $this->load->view('navbar', $data);
		    $this->load->view('council');
        $this->load->view('unca');
        $this->load->view('footer');
	}

    public function unsc()
	{
        $this->load->helper('url');
        $this->load->view('header');
        $data['currenturl'] = $this->uri->uri_string();
        $this->load->view('navbar', $data);
		    $this->load->view('council');
        $this->load->view('unsc');
        $this->load->view('footer');
	}

    public function unep()
	{
        $this->load->helper('url');
        $this->load->view('header');
        $data['currenturl'] = $this->uri->uri_string();
        $this->load->view('navbar', $data);
		    $this->load->view('council');
        $this->load->view('unep');
        $this->load->view('footer');
	}

    public function sochum()
	{
        $this->load->helper('url');
        $this->load->view('header');
        $data['currenturl'] = $this->uri->uri_string();
        $this->load->view('navbar', $data);
		    $this->load->view('council');
        $this->load->view('sochum');
        $this->load->view('footer');
	}

    public function undp()
	{
        $this->load->helper('url');
        $this->load->view('header');
        $data['currenturl'] = $this->uri->uri_string();
        $this->load->view('navbar', $data);
		    $this->load->view('council');
        $this->load->view('undp');
        $this->load->view('footer');
	}

    public function topic()
	{
        $this->load->helper('url');
        $this->load->view('header');
        $data['currenturl'] = $this->uri->uri_string();
        $this->load->view('navbar', $data);
        $this->load->view('topic');
        $this->load->view('footer');
	}

  public function studyguide()
{
      $this->load->helper('url');
      $this->load->view('header');
      $data['currenturl'] = $this->uri->uri_string();
      $this->load->view('navbar', $data);
      $this->load->view('studyguide');
      $this->load->view('footer');
}

}
