<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error404 extends CI_Controller {
    public function index()
	{
        $this->load->helper('url');
        $this->output->set_status_header('404');
        $this->load->view('header');
        $data['currenturl'] = $this->uri->uri_string();
        $this->load->view('navbar', $data);
        $this->load->view('error');
        $this->load->view('footer');
	}
}
