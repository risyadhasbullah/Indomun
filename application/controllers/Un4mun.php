<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Un4mun extends CI_Controller {

	public function index()
	{
        $this->load->helper('url');
        $this->load->view('header');
        $data['currenturl'] = $this->uri->uri_string();
        $this->load->view('navbar', $data);
		$this->load->view('un4mun');
        $this->load->view('footer');
	}
}
