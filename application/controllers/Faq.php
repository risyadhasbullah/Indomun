<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

	public function index()
	{
        $this->load->helper('url');
        $this->load->view('header');
        $this->load->view('faq-header');
        $data['currenturl'] = $this->uri->uri_string();
        $this->load->view('navbar', $data);
		$this->load->view('faq');
        $this->load->view('footer');
	}
}
