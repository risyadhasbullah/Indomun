<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	public function index()
	{
        $this->load->helper('url');
        $this->load->view('header');
        $data['currenturl'] = $this->uri->uri_string();
        $this->load->view('navbar', $data);
		$this->load->view('about');
        $this->load->view('footer');
	}
    
    public function roadshow()
	{
        $this->load->helper('url');
        $this->load->view('header');
        $data['currenturl'] = $this->uri->uri_string();
        $this->load->view('navbar', $data);
		$this->load->view('roadshow');
        $this->load->view('footer');
	}
  
    public function past()
	{
        $this->load->helper('url');
        $this->load->view('header');
        $data['currenturl'] = $this->uri->uri_string();
        $this->load->view('navbar', $data);
		$this->load->view('past');
        $this->load->view('footer');
	}
}
