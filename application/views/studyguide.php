<style>
  body{
    position: static;
    padding-bottom : 16vh;
  }
</style>
<!-- section undp Start	-->
<section id="undp" class="section-padding" style="padding-bottom: 0;">
    <div class="container">
        <div class="col-md-12 col-sm-12 wow fadeInUp" data-wow-delay='.4s'>
            <div class="about-intro">
                <div class="row">
                    <h2 class="center">Study guide</h2>
                    <p>Click on the documents below to view and download your study guide.</p>

                    <a href="https://drive.google.com/open?id=0ByeYqAPdhZAHVnVnczJOZmtEbUU">ROP</a><br />
										<a href="https://drive.google.com/open?id=0ByeYqAPdhZAHOS02eHgzcHdtVlE">SOCHUM</a><br />
										<a href="https://drive.google.com/open?id=0ByeYqAPdhZAHcEM5LTFUUnJmNFU">UNDP</a><br />
										<a href="https://drive.google.com/open?id=0ByeYqAPdhZAHdDlCXzJxbXVORUk">UNEP</a><br />
										<a href="https://drive.google.com/open?id=0ByeYqAPdhZAHeGgyUFdmT21ocHc">UNSC</a><br />
                    <a href="https://drive.google.com/open?id=0ByeYqAPdhZAHZG43NF9lTTZoSVk">ARF</a><br />
										<a href="https://drive.google.com/open?id=0ByeYqAPdhZAHeGlWVjZFVFczQ2M">ARF Chairman's statement</a><br />
                    <a href="https://drive.google.com/open?id=0ByeYqAPdhZAHQkVCdlZtVWg5Y1E">UN General Assembly (UN4MUN)</a></br>
                    <a href="https://drive.google.com/open?id=0ByeYqAPdhZAHSWpnVDRlWERyNFU">Press Study Guide</a>
                </div>
            </div>
        </div><!-- col-6  -->
    </div>
    <hr>
</section>
