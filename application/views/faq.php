<style>
  body{  
    min-height: calc(100% + 4em);
    margin-top: -4em;
    padding-top: 4em;
    position: relative;
    padding-bottom: 8vh;
  }
  #section-footer{
        position: absolute; 
        left: 0 ; right: 0; bottom: 0;
        padding-top: 6vh !important;
        padding-bottom: 2vh !important;
    }
</style>
  <section class="cd-faq">
      <ul class="cd-faq-categories">
          <li><a class="selected" href="#general">General questions</a></li>
          <li><a href="#accomodations">Accomodations</a></li>
          <li><a href="#Conferences">Conferences</a></li>
          <li><a href="#Registration">Registration</a></li>
      </ul> <!-- cd-faq-categories -->

      <div class="cd-faq-items">
          <ul id="general" class="cd-faq-group">
              <li class="cd-faq-title"><h2>GENERAL QUESTIONS</h2></li>
              <li>
                  <a class="cd-faq-trigger" href="#0">	Why IndonesiaMUN?</a>
                  <div class="cd-faq-content">
                      <p>IndonesiaMUN is the oldest and most prestigious MUN in Indonesia which upholds the innovative side of youths and is the perfect place where you can spend a weekend discussing important geopolitical and social issues and experience a comprehensive MUN conferences. The Annual 8th Session on November is a unique platform for you to meet, discuss, and collaborate with equally motivated and talented peers on the most relevant issues with substance excellence from across the globe. IndonesiaMUN has also made breakthroughs to pioneer events other than conferences such as I Care Campaign, Diplomatic Dinner, Panel Discussion, and BeyondMUN to Indonesia which you can experience this year.</p>
                  </div> <!-- cd-faq-content -->
              </li>

              <li>
                  <a class="cd-faq-trigger" href="#0">	Who organised Indonesia MUN?</a>
                  <div class="cd-faq-content">
                      <p>IndonesiaMUN is organised by UI MUN Club, which is the pioneer of MUN club in Indonesia. It has garnered reputation of highly skilled delegates shown by the numerous achievements in both national and international conferences.</p>
                  </div> <!-- cd-faq-content -->
              </li>
          </ul> <!-- cd-faq-group -->

          <ul id="accomodations" class="cd-faq-group">
              <li class="cd-faq-title"><h2>ACCOMMODATIONS, LOGISTICS, AND VISAS</h2></li>
              <li>
                  <a class="cd-faq-trigger" href="#0">	Is accommodation provided by the committee?</a>
                  <div class="cd-faq-content">
                      <p>No, IndonesiaMUN does not provide accommodation nor is it included in the delegate fee. However, we have partnered with various places around the venue to provide plenty of lodging options.</p>
                  </div> <!-- cd-faq-content -->
              </li>

              <li>
                  <a class="cd-faq-trigger" href="#0">	Does IndonesiaMUN provide meals and transportation?</a>
                  <div class="cd-faq-content">
                      <p>Yes, we provide lunches and snacks throughout our conferences. Transportation will be provided if there are any side events.</p>
                  </div> <!-- cd-faq-content -->
              </li>

              <li>
                  <a class="cd-faq-trigger" href="#0">	Can Indonesia MUN provide me an invitation letter to be given to my school/university or to support my visa application?</a>
                  <div class="cd-faq-content">
                      <p>IndonesiaMUN’s secretariat would be happy to provide an invitation letter for the delegate/delegations that have registered, been approved and paid their fees. To request one, please email marketing@indonesiamun.com with subject: Request for Invitation Letter.</p>
                  </div> <!-- cd-faq-content -->
              </li>
          </ul> <!-- cd-faq-group -->

          <ul id="Conferences" class="cd-faq-group">
              <li class="cd-faq-title"><h2>CONFERENCES AND ACTIVITIES</h2></li>
              <li>
                  <a class="cd-faq-trigger" href="#0">	Are there any activities besides the formal council meetings?</a>
                  <div class="cd-faq-content">
                      <p>Yes, there are some social events which can be checked in the Conference > Events tab in the website. All places will be informed so stay tuned.</p>
                  </div> <!-- cd-faq-content -->
              </li>

              <li>
                  <a class="cd-faq-trigger" href="#0">	Will there be a dress code during the conference?</a>
                  <div class="cd-faq-content">
                      <p>A formal dress code will be strictly enforced during the conference. Business apparel is required during all sessions, meaning suits for men and business dress for women. Casual wear is suitable for all other events.</p>
                  </div> <!-- cd-faq-content -->
              </li>
          </ul> <!-- cd-faq-group -->

          <ul id="Registration" class="cd-faq-group">
              <li class="cd-faq-title"><h2>REGISTRATION 	&amp; PAYMENT</h2></li>
              <li>
                  <a class="cd-faq-trigger" href="#0">	Is it possible to register for double delegation?</a>
                  <div class="cd-faq-content">
                      <p>Yes, it is possible for ASEAN Regional Forum and Security Council conferences.</p>
                  </div> <!-- cd-faq-content -->
              </li>

              <li>
                  <a class="cd-faq-trigger" href="#0">	Is it possible to get refunds for any plausible cause?</a>
                  <div class="cd-faq-content">
                      <p>No, it is not possible to refund your delegate fee. However, you can transfer your delegation to another person by contacting our registration team firsthand.</p>
                  </div> <!-- cd-faq-content -->
              </li>

              <li>
                  <a class="cd-faq-trigger" href="#0">	How much is the delegate fee? What is included in the fee?</a>
                  <div class="cd-faq-content">
                      <p>There are three types of registration: Early Bird, Priority Registration, Regular Registration, and Late registration. The Early Bird Registration will cost IDR 750,000,00 and it includes:<br>
                        a.	Committee sessions<br>
                        b.	Meals and snacks<br>
                        c.	Committee dinner<br>
                        d.	Transportation (during the side events)<br>
                        e.	I Care Campaign<br>
                        f.	Networking Night</p>
                  </div> <!-- cd-faq-content -->
              </li>

              <li>
                  <a class="cd-faq-trigger" href="#0">	How do I register for the conference?</a>
                  <div class="cd-faq-content">
                      <p>a.	First, create an account on the MyMUN website – or if you already have one, simply log in on the website.<br>
                        b.	Open our MyMUN page on mun.events/imun-2017/apply.<br>
                        c.	Choose the kind of delegation you would like to apply as (Single or Group).<br>
                        d.	Fill in the requirements on the website<br>
                        e.	Fill in the assignment preference, you may submit up to 3 (three) councils.<br>
                        f.	Submit your application.<br>
                        g.	If you have any inquires or more information, kindly visit mun.events/imun-2017</p>
                  </div> <!-- cd-faq-content -->
              </li>

              <li>
                  <a class="cd-faq-trigger" href="#0">	How do I prepare for the conference?</a>
                  <div class="cd-faq-content">
                      <p>a.	After you register, complete the procedure for the registration fee payment. Please kindly follow the registration fee payment guideline.<br>
                      b.	Kindly wait for several working days as the registration team processes your application. Then, you will receive an email stating the acceptance of your application, meaning that your payment has been verified and your application is now proceeded to the council(s) and country allocation<br>
                      c.	Wait for the allocation of your assigned country and council<br>
                      d.	If you got accepted, you’ll receive an email regarding which council you would participate in and which country you would represent.</p>
                  </div> <!-- cd-faq-content -->
              </li>

              <li>
                  <a class="cd-faq-trigger" href="#0">	Can secondary school students attend IMUN 2017?</a>
                  <div class="cd-faq-content">
                      <p>Unfortunately, we only accept applications from high school and university – both undergraduate and master degree – students.</p>
                  </div> <!-- cd-faq-content -->
              </li>
          </ul> <!-- cd-faq-group -->
      </div> <!-- cd-faq-items -->
      <a href="#0" class="cd-close-panel">Close</a>
  </section> <!-- cd-faq -->
  <script src="<?php echo base_url(); ?>js/faq/jquery-2.1.1.js"></script>
  <script src="<?php echo base_url(); ?>js/faq/jquery.mobile.custom.min.js"></script>
  <script src="<?php echo base_url(); ?>js/faq/faq.js"></script> <!-- Resource jQuery -->