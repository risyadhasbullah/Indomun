<link href='https://fonts.googleapis.com/css?family=Droid+Serif|Open+Sans:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo base_url(); ?>/css/timeline_reset.css"> <!-- CSS reset -->
<link rel="stylesheet" href="<?php echo base_url(); ?>/css/timeline.css"> <!-- Resource style -->
<script src="<?php echo base_url(); ?>/js/modernizr.js"></script>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">
<style>
    .cd-btn {
      display: inline-block;
      padding: 1.2em 1.4em;
      font-size: 1.3rem;
      color: #ffffff;
      text-transform: uppercase;
      font-weight: bold;
      letter-spacing: 1px;
      background-color: #202e4a;
      border-radius: .25em;
      margin-right: 1.5em;
    }
    .cd-btn:hover{
        color: white;
    }
    .cd-btn:nth-of-type(2) {
      margin-right: 0;
    }
    .cd-btn.main-action {
      background-color: #006ff5;
    }
    @media only screen and (min-width: 480px) {
      .cd-btn {
        padding: 1.2em 1.6em;
      }
    }
    @media only screen and (min-width: 768px) {
      .cd-btn {
        padding: 1.4em 1.8em;
      }
    }
</style>