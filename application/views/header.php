<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>IndonesiaMUN</title>
	<link rel="icon" type="image/gif" href="<?php echo base_url(); ?>/img/logo.png" />
	<!-- MAIN CSS FILES===================================
	======================================================= -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/css/line-icons.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/css/owl.theme.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/css/owl.transitions.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/css/style.css">
	<!-- CUSTOM STYLE	-->
	<link id="theme" rel="stylesheet" href="<?php echo base_url(); ?>/css/theme.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/css/responsive.css">
	<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->