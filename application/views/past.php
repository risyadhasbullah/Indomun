<!-- section past Start	-->
<section id="past" class="section-padding" style="padding-bottom: 0;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 wow fadeInUp" data-wow-delay='.4s'>
                <div class="about-intro text-center">
                    <h3>Past IndonesiaMUN</h3>
                </div>
            </div>
            <div class="embed-responsive embed-responsive-16by9 col-xs-12 text-center">
              <iframe style="margin: auto;" src="https://drive.google.com/file/d/0B0fBUSQhbCfHdkNvcEw1eXRUd2M/preview" width="640" height="480"></iframe>  
          </div>  
        </div><!-- row end  -->
    </div>
    <hr>
</section>