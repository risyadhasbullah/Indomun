	<!-- section footer strat -->
	<section id="section-footer">
      <footer>
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="footer-content">
						<ul class="list-inline footer-social wow fadeInUp" data-wow-delay=".3s">
							<li><a href="https://www.facebook.com/indonesiamun/"><i class="fa fa-facebook"></i></a></li>
							<li><a href="https://twitter.com/IndonesiaMUN"><i class="fa fa-twitter"></i></a></li>
							<li><a href="https://www.instagram.com/indonesiamun/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div><!-- row end  -->
		</div><!-- container end  -->
      </footer>
	</section>
	<!-- section Footer end -->
	
	<!-- Jquery plugin files	-->
	<script src="<?php echo base_url(); ?>/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>/js/jquery.easing.1.3.min.js"></script>
	<script src="<?php echo base_url(); ?>/js/owl.carousel.js"></script>
	<script src="<?php echo base_url(); ?>/js/wow.min.js"></script>
	<!-- CUSTOM JS FILE	-->
	<script src="<?php echo base_url(); ?>/js/custom.js"></script>
	<script src="<?php echo base_url(); ?>/js/main.js"></script>
</body>
</html>