<style>
#main-banner {
	background: url("<?php echo base_url(); ?>/img/crystal_of_knowledge.jpg") fixed 50% 50%;
    
    background-repeat: no-repeat;
}
</style>
<section class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="banner-main text-center" style="color:black !important; padding: 0; padding-top: 3em;">
                    <h3 class="lg-title wow fadeInLeft unselectable" >Venue</h3>
                </div>
            </div>
        </div>
    </div>
</section>

<!--  banner slider end  -->
<section id="main-banner">
</section>
<!-- section slider end	-->
<section class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="banner-main text-center" style="color:black !important; padding: 0;">
                    <p class="wow fadeInLeft unselectable">University of Indonesia is the oldest state university in Indonesia. It was established in 1849 with the name School to Opleiding van Inlandsche Artsen, as it was first founded by the Dutch government. The main campus is currently located in Depok, West Java. Throughout the history, University of Indonesia has been the number one destination to study at by hundreds of thousands students in the country, and it is also a university where numerous notable alumni had graduated, such as Soe Hok Gie, Chairul Tanjung, Indonesian Finance Minister Sri Mulyani,  Dr. Hatta, the first vice president of Indonesia, and Jimly Asshiddiqie, the first Chief of the Constitutional Court.<br><br>
 
                    The campus with 14 faculties is also well known by its comprehensive facilities. As a World Class University, University of Indonesia’s  library is the largest in South East Asia. It has a collection of more than 1,500,000 books. Yellow buses, the local term for the campus buses, exist as the main mode of transportation within the campus. University of Indonesia also has its own polyclinic, which provides free health care for all students.<br><br>

                    Another famous icon of the university is the Balairung Universitas Indonesia, the biggest building within the campus area that has the capacity to accommodate more than 5000 people.<br><br>
 
                    IndonesiaMUN 2017 will be held in the campus of University of Indonesia, Depok. We believe that it is the perfect place to welcome all delegates to participate in the biggest and most prestigious MUN event in Indonesia, as it provides you with the most outstanding facilities.</p>
                    
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.2006921868033!2d106.82737001490185!3d-6.368068564066802!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69ec1a804e8b85%3A0xd7bf80e1977cea07!2sUniversity+of+Indonesia!5e0!3m2!1sen!2sid!4v1497781458843" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div><!-- row end  -->
    </div><!-- container end  -->
    </div>
    </section>