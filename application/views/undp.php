<style>
#main-banner {
	background: url("<?php echo base_url(); ?>/img/council/migration.jpg") fixed 50% 50%;
    background-repeat: no-repeat;
}
</style>
<!-- section undp Start	-->
<section id="undp" class="section-padding" style="padding-bottom: 0;">
    <div class="container">
        <div class="col-md-5 col-sm-12 wow fadeInUp" data-wow-delay='.4s'>
            <div class="about-intro">
                <div class="row">
                    <h2 class="center">UNDP Council</h2>
                    <p>United Nation Development Programme (UNDP) works to achieve the eradication of poverty & the reduction of inequalities and exclusion. UNDP help countries to develop policies, leadership skills, institutional capabilities and build resilience in order to sustain development results. They also help countries to achieve the new Sustainable Development Goals (SDGs), which will guide global development priorities for the next 15 years.</p>

                    <p>For this year's UNDP council, we are honored to have Kadek Denny Baskara Adiputra from the Faculty of Law as our director. He won "Best Delegate for UNDP" on the previous IndonesiaMUN and also one of the delegates for Harvard National Model UN 2017! Besides that, he was chosen as the delegate of Ecosperity Young Leader's Dialogue in Singapore during 2016.</p>

                    <p>Nadifa Ramadhanty (2015) and Siti Rizqi Ashfina Rahmaddina (2014) both come from Faculty of Politics and Social Sciences and they will assist Denny Baskara in the success of this year's UNDP Council. Nadifa was one of the delegates for TEIMUN 2017 and Siti Rizqi was chosen as one of the delegates for Singapore MUN 2017.</p>
                </div>
            </div>
        </div><!-- col-6  -->
        <div class="col-md-7 four-images">
            <div class="row wow fadeInDown" data-wow-delay='.4s' style="margin-left: auto; margin-right: auto;">
                <img class="fix-margin" src="<?php echo base_url(); ?>/img/council/undp-logo.jpg" alt="undp" style="height:15em;">
                <img src="<?php echo base_url(); ?>/img/council/undp1.jpg" alt="undp" style="height:15em;">
            </div>
            <div class="row wow fadeInUp" data-wow-delay='.4s'>
                <img class="fix-margin" src="<?php echo base_url(); ?>/img/council/undp2.jpg" alt="undp" style="height:15em;">
                <img src="<?php echo base_url(); ?>/img/council/undp3.jpg" alt="undp" style="height:15em;">
            </div>
        </div>
    </div>
    <hr>
</section>