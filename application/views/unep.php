<style>
#main-banner {
	background: url("<?php echo base_url(); ?>/img/council/waste.jpg") fixed 50% 50%;
    background-repeat: no-repeat;
}
</style>
<!-- section unep Start	-->
<section id="unep" class="section-padding" style="padding-bottom: 0;">
<div class="container">
    <div class="col-md-5 col-sm-12 hidden-sm wow fadeInUp" data-wow-delay='.4s'>
        <div class="about-intro">
            <div class="row">
                <h2 class="center">UNEP Council</h2>
                <p>The United Nations Environment Programme (UNEP) promotes the coherent implementation of the environmental dimension of sustainable development within the United Nations system and serves as an authoritative advocate for the global environment. Today, UNEP is evolving around the issues of climate change, disaster and conflict response, ecosystem management, environmental governance and resource efficiency; as well as strengthening its institutions for the wise management of the environment.</p>

                <p>We are thrilled to have Mayang Krisnawardhani as our director for UNEP this year! As someone who comes from the Social Welfare background since 2013, she has a lot of experiences regarding environmental issues. In the realm of environment, She was the junior assistant in the Indonesia-Netherlands Water Management Cooperation, the presenter in the Climate Reality Project International, and is the advisor of Youth for Climate Change Indonesia. Mayang was the former Secretary General of University of Indonesia Model United Nations Club. She won several prestigious awards, such as Best Delegate for IndonesiaMUN 2015 & Padjadjaran Model United Nations 2014, Honorable Mention for Asia-Pacific Model United Nations 2016. She was also the Head Delegate of University of Indonesia for Harvard World MUN 2015 and was the chair of Harvard WorldMUN 2017 in Montreal. Soon she will be the Chair of AMUNC 2017.</p>

                <p>Along with Mayang, Mendra Roberto (Faculty of Economics 2015) & Cut Farisa Machmud (Faculty of Economics 2014). Mendra received Bronze Medalist from National Economics Olympiad, given by Minister of Education and Culture in 2014. He was also won the Most Outstanding Delegate Award of Nanyang Technological University Model United Nation, Singapore 2016 & Best Speaker and Grandfinalist of Indonesia Investment Banking Competition, University of Prasetiya Mulya, Indonesia, 2016. He is now part of UI for Asia Pacific Model UN Conference 2017. Farisa won Verbal Commendation on Nanyang Technological University Model United Nation, Singapore 2016 & champion at Indonesian Marketing Competition, Universitas Indonesia, 2016!</p>
            </div>
        </div>
    </div><!-- col-6  -->
    <div class="col-md-7 four-images">
        <div class="row wow fadeInDown" data-wow-delay='.4s'>
            <img class="fix-margin" src="<?php echo base_url(); ?>/img/council/unep-logo.jpg" alt="undp" style="height:15em;">
            <img src="<?php echo base_url(); ?>/img/council/unep1.jpg" alt="undp" style="height:15em;">
        </div>
        <div class="row wow fadeInUp" data-wow-delay='.4s'>
            <img class="fix-margin" src="<?php echo base_url(); ?>/img/council/unep2.jpg" alt="undp" style="height:15em;">
            <img src="<?php echo base_url(); ?>/img/council/unep3.jpg" alt="undp" style="height:15em;">
        </div>
    </div>
</div>
<hr>
</section>