<style>
.error-template {padding: 40px 15px;text-align: center;}
.error-actions {margin-top:15px;margin-bottom:15px;}
.error-actions .btn { margin-right:10px; }
    body{
        min-height: 100vh;
        padding-top: 8em;
        position: relative;
        padding-bottom: 8vh;
        display: flex;
        flex-direction: column;
    }
    #section-footer{
        position: absolute;
        left: 0 ; right: 0; bottom: 0;
        padding-top: 6vh !important;
        padding-bottom: 2vh !important;
    }
    .container{
      flex: 1;
    }
</style>

<div class="container">
    <div class="row">
    <div class="error-template">
	    <h1>Oops!</h1>
	    <h2>404 Not Found</h2>
	    <div class="error-details">
		Sorry, an error has occured, Requested page not found!<br>
		<?php echo $message; ?>
	    </div>
	    <div class="error-actions">
		<a href="<?php echo base_url(); ?>" class="btn btn-primary">
		    <i class="icon-home icon-white"></i> Take Me Home </a>
	    </div>
	</div>
    </div>
</div>
