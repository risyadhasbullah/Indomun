<style>
#main-banner {
	background: url("<?php echo base_url(); ?>/img/council/maritime.jpg") fixed 50% 50%;
    background-repeat: no-repeat;
}
</style>
<!-- section arf Start	-->
<section id="arf" class="section-padding" style="padding-bottom: 0;">
    <div class="container">
        <div class="col-md-5 col-sm-12 hidden-sm wow fadeInUp" data-wow-delay='.4s'>
            <div class="about-intro">
                <div class="row">		
                    <h2 class="center">ARF Council</h2>
                    <p>ASEAN Regional Forum (ARF) is a forum to promote open dialogue on political and security cooperation in the Asia-Pacific region. As one of the ASEAN Political-Security Community, ARF was established with two objectives, first is to foster constructive dialogue and consultation on political and security issues of common interest and concern; and second is to make significant contributions to efforts towards confidence-building and preventive diplomacy in the Asia-Pacific region.</p>

                    <p>This year's ASEAN Regional Forum is directed by Steven Giovanni Sugiarto from Faculty of Economics 2015. His achievements regarding MUN are undoubtedly extraordinary, such as being Verbal Commendation of NTU MUN 2016, the Best Delegate of President MUN 2016 and UI's official delegation to Harvard WorldMUN 2017.</p>

                    <p>Steven also works with two brilliant assistant directors: Latasha Desideria from Faculty of Economics 2014 and Adizsa Nurulhuda from Faculty of Psychology 2016. Latasha is the Verbal Commendation for 14th Economix MUN 2016, Honotable Mention of SEMUN 2017 and the delegate for European MUN 2017, while Adizsa is the Best Delegate for UI UNday MUN 2014, Best Delegate for HighScope MUN 2014, Verbal Commendation for IndonesiaMUN 2016 and NTU MUN 2017</p>
                </div>
            </div>
        </div><!-- col-6  -->
        <div class="col-md-7 four-images">
            <div class="row wow fadeInDown" data-wow-delay='.4s'>
                <img class="fix-margin" src="<?php echo base_url(); ?>/img/council/ARF-logo.jpg" alt="undp" style="height:15em;">
                <img src="<?php echo base_url(); ?>/img/council/ARF1.jpg" alt="undp" style="height:15em;">
            </div>
            <div class="row wow fadeInUp" data-wow-delay='.4s'>
                <img class="fix-margin" src="<?php echo base_url(); ?>/img/council/ARF2.jpg" alt="undp" style="height:15em;">
                <img src="<?php echo base_url(); ?>/img/council/ARF3.jpg" alt="undp" style="height:15em;">
            </div>
        </div>
    </div>
    <hr>
</section>