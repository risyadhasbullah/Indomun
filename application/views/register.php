<section class="section-padding" style="background-color:white;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="banner-main text-center" style="color:black !important; padding: 0; padding-top:3em;">
                    <h3 class="lg-title wow fadeInLeft unselectable" >Registration</h3>
                    <p class="wow fadeInLeft unselectable">Today is the day!<br><br>

                    The moment you've been waiting for is finally here -- today, you can finally be a part of the biggest MUN event in Indonesia with the most exclusive deal. With IDR 850.000,00 for National Delegates and US $80 for International Delegates, you can experience the life of a diplomat for five days, engage with many people from around the world, and be empowered by the various side events that we offer.<br><br>

                    There will be many others who are as excited as you, so make sure to follow the steps properly, watch out for the requirements, and be one of the lucky early delegates. Don’t forget to pay attention to the prices in our MyMUN Description Page and our website before you register.<br><br>

                    Following your dream starts now!</p>
                    <a href="https://mymun.net/muns/imun-2017" class="cd-btn main-action wow fadeInLeft">Register now</a><br><br>
                </div>
                <div class="banner-main" style="color:black !important; padding: 0; padding-top:3em;">

                    <h3>National Delegate Registration Fee :</h3><br><br>

                    <p>1. Regular Registration (1 September - 30 September) IDR 850.000,<br>

                    2. Late Registration (1 October - 15 October) IDR 900.000,<br>

                    3. Observer/ Faculty Advisor IDR 450.000.</p><br><br>

                    <h3>International Delegate Registration Fee :</h3><br><br>

                    <p>1. Regular (23 July - 15 October) US $ 80,<br>

                    2. Observer/ Faculty Advisor US $ 50.<br>

                    <h3>National Delegation Registration Fee :</h3><br><br>

                    <p>1. Regular Registration (1 September - 30 September) IDR 820.000,<br>

                    2. Late Registration (1 October - 15 October) IDR 850.000,<br>

                    3. Observer/ Faculty Advisor IDR 450.000.</p><br><br>

                    <h3>International Delegation Registration Fee :</h3><br><br>

                    <p>1. Regular (23 July - 15 October) US $ 76,<br>

                    2. Observer/ Faculty Advisor US $ 50.<br><br>

                    Small Delegation : 4-6 Persons,<br>
                    Big Delegation : Minimum 7 Persons.<br><br>
                      Double Delegation is ONLY available for UNSC and Asean Regional Forum.<br><br></p>

                    <h3 class="text-center">Below are the registration steps for single delegate</h3>
            </div>
        </div><!-- row end  -->
    </div><!-- container end  -->
    </div>
</section>
<section id="cd-timeline" class="cd-container">
<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-picture">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 1</h2>
        <p>Click "Register now" button in the above section of this page.</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-movie">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 2</h2>
        <p>You'll be headed to IMUN 2017 MyMUN Page and click "Apply Now" after you create your MyMUN account</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-location">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 3</h2>
        <p> Click the "Delegate" button for Single Delegate</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-picture">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 4</h2>
        <p>Fill our motivation letter and our application form; choose 1 - 3 committee preferences along with your preferred country</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-movie">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 5</h2>
        <p>Submit your application by clicking "Submit Application" button or save your application as a draft by clicking "Save as a Draft" button if there are things you want to change or add later on</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-location">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 6</h2>
        <p>You will receive a follow-up email that will notify you if your application have been submitted. You'll continue to the payment procedure</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-picture">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 7</h2>
        <p>Recheck your MyMUN application for IndonesiaMUN and click the payment option tab. Click "ORDER" on the fee that you need to pay (based on your chosen registration phase). Select according to the options provided by MyMUN</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-movie">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 8</h2>
        <p>For national delegates, please pay according to the payment fee  listed in this page or you can also view them in our introduction page of mymun. For international delegate, please pay referring to the payment option of MyMUN (using US Dollar).</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-location">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 9</h2>
        <p>After you already ordered a fee, it will pop up on your shopping cart icon. Then, choose your preferred payment procedure (Wire Transfer or  PayPal).</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-picture">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 10</h2>
        <p>You will receive a follow-up email that will notify you if your application have been submitted. You'll continue to the payment procedure</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-movie">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 11</h2>
        <p>When you have made your payment, upload a picture of your payment proof</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-location">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 12</h2>
        <p>You will receive a follow-up email that will notify you that your application have been accepted</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

</section> <!-- cd-timeline -->

<section class="section-padding" style="background-color:white;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="banner-main text-center" style="color:black !important; padding: 0; padding-top:3em;">
                    <h3>Below are the registration steps for delegation</h3>
            </div>
        </div><!-- row end  -->
    </div><!-- container end  -->
    </div>
</section>

<section id="cd-timeline" class="cd-container">
<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-picture">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 1</h2>
        <p>Click "Register now" button in the above section of this page.</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-movie">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 2</h2>
        <p>You'll be headed to IMUN 2017 MyMUN Page and click "Apply Now" after you create your MyMUN account</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-location">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 3</h2>
        <p>Click the "Head Delegate" button for Delegation</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-picture">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 4</h2>
        <p>You need to create your Delegation by filling the form under "Your New Society" subheading. Do not forget to check your role as a Head Delegate and mention the size of your delegation. Click "Create Delegation" button at the bottom as you finished.</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-movie">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 5</h2>
        <p>Answer the application question for delegation and write the motivation letter</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-location">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 6</h2>
        <p>You will be redirected to your Delegation's profile page where you can invite members of your delegation by inserting their email.</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-picture">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 7</h2>
        <p>Fill your own motivation letter and our application form; choose 3 - 5 committee preferences along with your preferred country</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-movie">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 8</h2>
        <p>Also fill in the Motivation Letter of the delegation in the delegation page</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-location">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 9</h2>
        <p>Submit your application by clicking "Submit Application" button or save your application as a draft by clicking "Save as a Draft" button if there are things you want to change or add later on</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-picture">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 10</h2>
        <p>You will receive a follow-up email that will notify you if your application have been submitted. You'll continue to the payment procedure</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-movie">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 11</h2>
        <p>Recheck your MyMUN application for IndonesiaMUN and click the payment option tab. Click "ORDER" on the fee that you need to pay (based on your chosen registration phase). Select according to the options provided by MyMUN</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-location">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 12</h2>
        <p>For national delegates, please pay according to the payment fee  listed in this page or you can also view them in our introduction page of mymun. For international delegate, please pay referring to the payment option of MyMUN (using US Dollar).</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-picture">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 13</h2>
        <p>After you already ordered a fee, it will pop up on your shopping cart icon. Then, choose your preferred payment procedure (Wire Transfer or  PayPal).</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-movie">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 14</h2>
        <p>When you have made your payment, upload a picture of your payment proof</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->

<div class="cd-timeline-block">
    <div class="cd-timeline-img cd-location">
    </div> <!-- cd-timeline-img -->

    <div class="cd-timeline-content">
        <h2>Step 15</h2>
        <p>You will receive a follow-up email that will notify you that your application have been accepted</p>
    </div> <!-- cd-timeline-content -->
</div> <!-- cd-timeline-block -->
</section> <!-- cd-timeline -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>
jQuery(document).ready(function($){
	var timelineBlocks = $('.cd-timeline-block'),
		offset = 0.8;

	//hide timeline blocks which are outside the viewport
	hideBlocks(timelineBlocks, offset);

	//on scolling, show/animate timeline blocks when enter the viewport
	$(window).on('scroll', function(){
		(!window.requestAnimationFrame)
			? setTimeout(function(){ showBlocks(timelineBlocks, offset); }, 100)
			: window.requestAnimationFrame(function(){ showBlocks(timelineBlocks, offset); });
	});

	function hideBlocks(blocks, offset) {
		blocks.each(function(){
			( $(this).offset().top > $(window).scrollTop()+$(window).height()*offset ) && $(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
		});
	}

	function showBlocks(blocks, offset) {
		blocks.each(function(){
			( $(this).offset().top <= $(window).scrollTop()+$(window).height()*offset && $(this).find('.cd-timeline-img').hasClass('is-hidden') ) && $(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
		});
	}
});
</script>
