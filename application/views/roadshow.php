<style>
#main-banner {
	background: url("<?php echo base_url(); ?>/img/about/roadshow.jpg") fixed 0% 50%;
    background-repeat: no-repeat;
}
</style>
<!--  banner slider end  -->
<section id="main-banner">
</section>
<!-- section slider end	-->

<!-- section ABOUT Start	-->
<section id="roadshow" class="section-padding" style="padding-bottom: 0;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-12 hidden-sm wow fadeInUp" data-wow-delay='.4s'>
                <div class="about-intro">
                    <p>Greetings, future leaders!
                      As the excitement of IndonesiaMUN 2017 grows stronger, we want to involve you in joining the thrill.  Sign your schools and universities up on the journey of our roadshow session!<br><br>
                      
                      During our visit, we'll show you the benefits of joining the most prestigious MUN event in Indonesia and give you the exclusive chance to sign up for delegates on the spot.<br><br>
                      
                      So, what are you waiting for? Sign your institutes up to join the fun at <a href="http://tinyurl.com/SchoolRegistrationRoadshowIMUN">http://tinyurl.com/SchoolRegistrationRoadshowIMUN!</a></p>
                </div>
            </div>
        </div><!-- row end  -->
    </div>
    <hr>
</section>