<style>
#main-banner {
	background: url("<?php echo base_url(); ?>/img/council/trafficking.jpg") fixed 50% 50%;
    background-repeat: no-repeat;
}
</style>
<!-- section unca Start	-->
<section id="unca" class="section-padding" style="padding-bottom: 0;">
    <div class="container">
        <div class="col-md-6 col-sm-12 hidden-sm wow fadeInUp" data-wow-delay='.4s'>
            <div class="about-intro">
                <div class="row">
                    <h2 class="center">UNCA Council</h2>
                    <p>Press Corps/United Nation Correspondents' Association delegate represent notable international news agenices. They may hold a press conference in a committee at any time during a debate. They will ask questions to either the whole committee or specific delegates, based on the content of the debate. The Press Corps also publishes content on daily newsletter, their website and various social media accounts that cover what is happening in debate.</p>

                    <p>We are happy to announce that there will be two directors for the UNCA. Our first director is Firman Raharjo from Faculty of Engineering 2014. His achievements are remarkable! He won Best Delegate for IndonesiaMUN (SOCHUM) 2016 and Honorable Mention for NTU MUN 2016. He was also chosen as one of the delegates representing UI for NTU MUN 2016 and Harvard World MUN 2017!</p>

                    <p>Along with Firman, Tara Mecca Luna from Faculty of Computer Science 2016, has accomplished oustanding awards too! She also won Most Outstanding Delegate for IndonesiaMUN (UNDP) 2016 and Best Position Paper Indonesia MUN 2016. Tara is now one of the official delegates for Harvard World MUN 2017.</p>
                </div>
            </div>
        </div><!-- col-6  -->
        <div class="col-md-6">
            <div class="row wow fadeInUp" data-wow-delay='.4s'>
                <img class="center-image center" src="<?php echo base_url(); ?>/img/council/unca-logo.jpg" alt="unca" style="height:15em;">
            </div>
            <div class="row wow fadeInUp" data-wow-delay='.8s'>
                <img class="center-image center" src="<?php echo base_url(); ?>/img/council/unca1.jpg" alt="unca" style="height:15em;">
            </div>
            <div class="row wow fadeInUp" data-wow-delay='1.2s'>
                <img class="center-image center" src="<?php echo base_url(); ?>/img/council/unca2.jpg" alt="unca" style="height:15em;">
            </div>
        </div>
    </div>
    <hr>
</section>