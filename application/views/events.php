<style>
    body{
      min-height: calc(100% + 4em);
        margin-top: -4em !important;
        padding-top: 4em;
    }
    .tab-content{
        padding: 2em;
    }
    .wrapper{
        min-height: 71vh;
    }
    #events{
        padding-bottom: 0;
        margin-top: 5em;
        height: auto;
    }
    .tab-content img{
        width: 100%;
        height: auto;
    }
    @media (max-width : 767px){
        #events{
            margin-top: 8em;
        }
    }
    @keyframes slidy {
        0% { left: 0%; }
        28% { left: 0%; }
        33.333% { left: -100%; }
        61% { left: -100%; }
        66.666% { left: -200%; }
        94% { left: -200%; }
        100% { left: -300%; }
    }

    body { margin: 0; }
    div#slider { overflow: hidden; }
    div#slider figure img { width: 25%; float: left; }
    div#slider figure {
      position: relative;
      width: 400%;
      margin: 0;
      left: 0;
      text-align: left;
      font-size: 0;
      animation: 12s slidy infinite;
    }
    .munlogo{
        z-index: 1;
    }
    .wrapper{
        z-index: 0;
    }
  #section-footer{
    bottom: -5em;
  }

</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>/css/slider.css">

<!-- section events Start	-->
    <div class="wrapper">
        <section id="events" class="section-padding">
            <div class="container">
                <div class="row">
                  <ul class="nav nav-pills">
                    <li class="active"><a data-toggle="pill" href="#campaign">I care campaign</a></li>
                    <li><a data-toggle="pill" href="#diplomatic-dinner">Diplomatic dinner</a></li>
                    <li><a data-toggle="pill" href="#committee-dinner">Committee dinner</a></li>
                    <li><a data-toggle="pill" href="#networking">Networking night</a></li>
                    <li><a data-toggle="pill" href="#discussion">Panel discussion</a></li>
                    <li><a data-toggle="pill" href="#beyondMUN">Beyond MUN</a></li>
                    <li><a data-toggle="pill" href="#expert">Experts briefing</a></li>
                  </ul>

                  <div class="tab-content">
                    <div id="campaign" class="tab-pane fade in active">
                        <div id="slider">
                            <figure>
                                <img src="<?php echo base_url(); ?>/img/events/I_care_3.jpg" alt="I care campaign">
                                <img src="<?php echo base_url(); ?>/img/events/I_care_1.jpg"  alt="I care campaign">
                                <img src="<?php echo base_url(); ?>/img/events/I_care_2.jpg"  alt="I care campaign">
                                <img src="<?php echo base_url(); ?>/img/events/I_care_3.jpg" alt="I care campaign">
                            </figure>
                        </div>
                        <br><br>
                        <p>IndonesiaMUN 2017 will act as the first IndonesiaMUN to provide the platform for delegates to participate directly in our social-related campaign and impact the society passionately! We will cooperate with relevant entities namely NGOs, foundations, and governmental agencies in molding the campaign.</p>
                    </div>

                    <div id="committee-dinner" class="tab-pane fade">
                        <div id="slider">
                            <figure>
                                <img src="<?php echo base_url(); ?>/img/events/committee_dinner_2.jpg" alt="committee dinner">
                                <img src="<?php echo base_url(); ?>/img/events/committee_dinner.jpg" alt="committee dinner">
                                <img src="<?php echo base_url(); ?>/img/events/committee_dinner_3.jpg" alt="committee dinner">
                                <img src="<?php echo base_url(); ?>/img/events/committee_dinner.jpg" alt="committee dinner">
                            </figure>
                        </div><br><br>
                        <p>Delegates will be placed in specific restaurants throughout the mall based on their committees. The purpose of this dinner is to be the ice breaker between delegates and chairs since this will be the first chance for them to get acquainted. Delegates and chairs could share personal stories regarding their experience of previous Model UN or any related international experience. Chairs could convey their expectations for the committee sessions in the upcoming days.</p>
                    </div>

                    <div id="diplomatic-dinner" class="tab-pane fade">
                        <div id="slider">
                            <figure>
                                <img src="<?php echo base_url(); ?>/img/events/diplomatic_dinner.jpg" alt="diplomatic dinner">
                                <img src="<?php echo base_url(); ?>/img/events/diplomatic_dinner_2.jpg" alt="diplomatic dinner">
                                <img src="<?php echo base_url(); ?>/img/events/diplomatic_dinner_3.jpg" alt="diplomatic dinner">
                                <img src="<?php echo base_url(); ?>/img/events/diplomatic_dinner.jpg" alt="diplomatic dinner">
                            </figure>
                        </div><br><br>
                        <p>Diplomatic dinner will be held on November 10, 2017 at Mawar Room, Balai Kartini. The evening is intended to provide you with an immersive experience of interacting and discussing with real-life diplomats and international figures about the pressing issue of migration of peoples. This year, we are bringing up the theme of “People in Movement: A World Without Walls.” Our notable speakers for this event are Eshila Mayanvika as director of UN Information Center Jakarta, Diovio Alfath as Founder and Executive Director Sandya Institute for Peace and Human Rights, and Ruby Khalifa as executive director of ASEAN Muslim Network.</p>
                    </div>

                    <div id="networking" class="tab-pane fade">
                        <!--<div id="slider">
                            <figure>
                                <img src="<?php echo base_url(); ?>/img/events/networking.jpg" alt="networking night">
                                <img src="<?php echo base_url(); ?>/img/events/networking_2.jpg" alt="networking night">
                                <img src="<?php echo base_url(); ?>/img/events/networking_3.jpg" alt="networking night">
                                <img src="<?php echo base_url(); ?>/img/events/networking.jpg" alt="networking night">
                            </figure>
                        </div>--><br><br>
                        <p style="padding-top:40%;">This is the time to relax from the committee sessions, and the opportunity bring delegates closer from all committees accompanied with electrifying music and beverages that will certainly make people dance and the night unforgettable.</p>
                    </div>

                    <div id="discussion" class="tab-pane fade">
                        <div id="slider">
                            <figure>
                                <img src="<?php echo base_url(); ?>/img/events/panel_discussion.jpg" alt="panel discussion">
                                <img src="<?php echo base_url(); ?>/img/events/panel_discussion_2.jpg" alt="panel discussion">
                                <img src="<?php echo base_url(); ?>/img/events/panel_discussion_3.jpg" alt="panel discussion">
                                <img src="<?php echo base_url(); ?>/img/events/panel_discussion.jpg" alt="panel discussion">
                            </figure>
                        </div><br><br>
                        <p>A thematic discussion focusing on “Pursuing Sustainable Indonesia : The Implementation of Sustainable Development Goal 17”. The panel will consist of four speakers and a moderator, with the composition one representative from the Indonesian Government, one representative from the United Nations, one representative from civil society groups, and one representative from the young generation of Indonesia. This discussion will stress on how stakeholders can contribute towards the multi-sectoral development of The Republic of Indonesia</p>
                    </div>

                    <div id="expert" class="tab-pane fade">
                        <div id="slider">
                            <figure>
                                <img src="<?php echo base_url(); ?>/img/events/experts_3.jpg" alt="Experts Briefing">
                                <img src="<?php echo base_url(); ?>/img/events/experts_3.jpg" alt="Experts Briefing">
                                <img src="<?php echo base_url(); ?>/img/events/experts_3.jpg" alt="Experts Briefing">
                                <img src="<?php echo base_url(); ?>/img/events/experts_3.jpg" alt="Experts Briefing">
                            </figure>
                        </div><br><br>
                        <p>Experts from United Nations agencies, governmental institutions, non-governmental organizations, and academia that were involved in the making of the Study Guide (a background guide of the selected topics to be discussed by committees) are involved at the beginning of the First Committee Session to brief delegates in their respective discussion to give a broader understanding of the topics.
</p>
                    </div>

                    <div id="beyondMUN" class="tab-pane fade">
                        <div id="slider">
                            <figure>
                                <img src="<?php echo base_url(); ?>/img/events/beyond_mun.jpg" alt="beyond mun">
                                <img src="<?php echo base_url(); ?>/img/events/beyond_mun_2.jpg" alt="beyond mun">
                                <img src="<?php echo base_url(); ?>/img/events/beyond_mun_3.jpg" alt="beyond mun">
                                <img src="<?php echo base_url(); ?>/img/events/beyond_mun.jpg" alt="beyond mun">
                            </figure>
                        </div><br><br>
                        <p>This will be a sharing session from the previous awardees of IndonesiaMUN, and the exemplary alumni of Universitas Indonesia Model United Nations club of how Model UN helps them to develop skills that are needed in both their personal and professional lives. </p>
                    </div>
                  </div>
                </div><!-- row end  -->
            </div>
        </section>
    </div>
