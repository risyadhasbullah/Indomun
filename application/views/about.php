<!--  banner slider end  -->
<section id="main-banner">
</section>
<!-- section slider end	-->


<!-- section ABOUT Start	-->
<section id="aboutMUN" class="section-padding" style="padding-bottom: 0;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-12 hidden-sm wow fadeInUp" data-wow-delay='.4s'>
                <div class="about-intro">
                    <h2 class="center">About MUN</h2>
                    <p>Model UN is an educational simulation and academic competition that are very popular for those interested in learning more about the UN and the art of diplomacy. It is estimated that more than 400,000 students worldwide participate every year in MUN at all educational levels – from primary school to university. Many of today’s leaders in law, government, business and the arts participated in MUN as students</p>
                    <p class="center">https://outreach.un.org/mun/</p>
                  
                    <p>Here's a short video of how Model UN works that we did in collaboration with the indonesian Youtube Sensation skinnyindonesian24. Don't forget to subscribe to their channel too!</p>

                    <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/2XOOiw5rKMw' frameborder='0' allowfullscreen></iframe></div>
                </div>
            </div>
        </div><!-- row end  -->
    </div>
    <hr>
</section>

<section id="indonesiaMUN" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 hidden-sm wow fadeInUp" data-wow-delay='.4s'>
                <div class="about-intro">
                    <h2 class="center">About IndonesiaMUN</h2>
                    <p>Indonesia Model United Nations (IMUN) is known to be the premier Model United Nations in Indonesia, organized by Universitas Indonesia for the 8th time in 2017. Our competition is a platform for participants ranging from high school to varsity students, from both national and International delegates. Since 2010, we have engaged youths in global issues and introduce them to the process of multilateral diplomacy in the United Nations. IndonesiaMUN was established to provide a room for students to practice diplomacy, negotiation, and legal drafting skills, so they can bring about betterment for their community by learning beyond classroom walls. The first IndonesiaMUN was successfully conducted in 2010, bringing 182 participants from not only nation-wide but also ASEAN. The second IndonesiaMUN held in 2011 had a vision in expanding the quantity as well as increasing the quality of the conference with 266 participants in 5 committees. The fourth IndonesiaMUN (2013) expanded even further by bringing 7 committees and piloting the first NGO Committee for high school delegates. The Sixth IndonesiaMUN pioneered the excursion to the Indonesia Peace and Security Center (IPSC) in Sentul and Diplomatic Dinner with real diplomats. The seventh IndonesiaMUN last year introduced new events such as the Expert Workshop and BeyondMUN. For seven years in a row IMUN has successfully spread the spirit of diplomacy to thousands of national and international students worldwide to countries such as the United States of America, Russian Federation, Singapore, Malaysia, and the Philippines. IndonesiaMUN is the only Model United Nations in Indonesia, which put effort in maintaining the substantive quality of the conference. Our Study Guides are strictly maintained and prepared with respect to integrity, keeping it as accurate as we can to the current facts and condition. This year, we intend to directly involve UN Agencies, Government Bodies, Non-Governmental Organizations, and experts in preparing our substance. Our Board of Dais, from time to time, are selected from award winners of numerous International MUNs, such as The Asia Pacific Model United Nations Conference (AMUNC), The European International Model United Nations (TEIMUN), Harvard World Model United Nations (HWMUN), Nanyang Technological Model United Nations (NTUMUN), SingaporeMUN (SMUN), EuropeanMUN (EuroMUN), and the World Federation of United Nations Associations (WFUNA) International Model United Nations (WIMUN). </p>
                </div>
            </div>
        </div><!-- row end  -->
    </div><!-- container end  -->
</section>
<!-- section ABOUT end	-->

<!--  UI MUN start  -->
<section id="uiMUN" class="section-padding" style="padding-bottom: 3em;">
    <div class="feature-heading-top ">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading text-center wow fadeInUp">
                        <img src="<?php echo base_url(); ?>/img/UIMUN.png" alt="IndonesiaMUN">
                        <p>UI MUN Club is a renowned student-run organization based in Universitas Indonesia which focuses on skills development of its 300 members, including negotiation, research, drafting, as well as delivering speeches in a diplomatic manner. Several annual programs that have been organized by this organization are Biweekly General Training, Mentoring, International and National Delegations, High School Model United Nations, and also the Grand General Assembly. Since its establishment, members of this organization have snatched many notable awards such as but not limited to: Best Delegate award, Diplomacy Award, Most Outstanding, and also Honorable Mention in Harvard National Model United Nations(HNMUN), Harvard World Model United Nations(HWMUN), Asia Pacific Model United Nations Conference (AMUNC), The European International Model United Nations(TEIMUN), Singapore Model United Nations(SMUN), European Model United Nations(EUROMUN), Nanyang Technological University Model United Nations(NTUMUN), KoreaMUN (KMUN), and the World Federation of United Nations Associations (WFUNA) Model United Nations (WIMUN). Ever since its inception, The Board of Secretariat for IndonesiaMUN has always been the exemplary members of UI MUN Club. In coordination IndonesiaMUN Board of Secretariat, UI MUN Club is committed in making IndonesiaMUN 2017 the best MUN in the Indonesian Archipelago and beyond. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--  UI MUN end  -->