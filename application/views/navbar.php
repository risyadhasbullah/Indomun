</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
	<!-- Preloader -->
	<div id="loader">
		<div class="icon"></div>
	</div>
	<!-- END: Preloader -->
	<!-- .site-header-navbar-->
	<header id="masthead" class="site-header site-header-fixed-top">
		<nav id="siteHeaderNavbar" class="site-header-navbar navbar navbar-lg navbar-fixed-top  navbar-bg-from-transparent navbar-fg-from-light navbar-default">
			<!-- Navigation -->
			<div class="container">
				<div class="row">
					<div class="navbar-header ">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse"> <i class="fa fa-bars"></i> </button>
						<a class="navbar-brand page-scroll " href="<?php echo base_url(); ?>">
							<img class="munlogo" src="<?php echo base_url(); ?>/img/logo.png" alt="Indonesia MUN"></a>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse  navbar-main-collapse navbar-right">
						<ul class="nav navbar-nav">
							<li class="dropdown">
							  <a class="page-scroll dropdown-toggle" data-toggle="dropdown">Home<span class="caret"></span></a>
							  <ul class="dropdown-menu">
                                <li><a class="page-scroll" href="<?php if ($currenturl == '') { echo '#IndonesiaMUN'; } else { echo site_url('#IndonesiaMUN');} ?>">IndonesiaMUN</a></li>
							    <li><a class="page-scroll" href="<?php if ($currenturl == '') { echo '#highlights'; } else { echo site_url('#highlights');} ?>">Highlights</a></li>
								<li><a class="page-scroll" href="<?php if ($currenturl == '') { echo '#letter'; } else { echo site_url('#letter');} ?>">Letter from Secretary General</a></li>
							  </ul>
							</li>
							<li class="dropdown">
					          <a class="page-scroll dropdown-toggle" data-toggle="dropdown">About<span class="caret"></span></a>
					          <ul class="dropdown-menu">
					            <li><a class="page-scroll" href="<?php if ($currenturl == 'about') { echo '#aboutMUN'; } else { echo site_url('/about/#aboutMUN');} ?>">About MUN</a></li>
					            <li><a class="page-scroll" href="<?php if ($currenturl == 'about') { echo '#indonesiaMUN'; } else { echo site_url('/about/#indonesiaMUN');} ?>">Indonesia MUN</a></li>
					            <li><a class="page-scroll" href="<?php if ($currenturl == 'about') { echo '#uiMUN'; } else { echo site_url('/about/#uiMUN');} ?>">UI MUN Club</a></li>
                                <li><a class="page-scroll" href="<?php echo site_url('/about/roadshow'); ?>">Roadshow</a></li>
                                <li><a class="page-scroll" href="<?php echo site_url('/about/past'); ?>">Past IndonesiaMUN</a></li>
					          </ul>
					        </li>
              <li><a href="<?php echo site_url('/preevents') ?>">Pre-events</a></li>
							<li><a href="<?php echo site_url('/un4mun') ?>">UN4MUN</a></li>
							<li class="dropdown">
					          <a class="page-scroll dropdown-toggle" data-toggle="dropdown">Conference<span class="caret"></span></a>
					          <ul class="dropdown-menu">
					            <li><a href="<?php echo site_url('/events') ?>">Events</a></li>
                                <li><a href="<?php echo site_url('/venue') ?>">Venue</a></li>
					          </ul>
					        </li>
							<li class="dropdown">
					          <a class="page-scroll dropdown-toggle" data-toggle="dropdown">Committees<span class="caret"></span></a>
					          <ul class="dropdown-menu">
					            <li><a href="<?php echo site_url('/committees/arf') ?>">ARF</a></li>
					            <li><a href="<?php echo site_url('/committees/SOCHUM') ?>">SOCHUM</a></li>
                                <li><a href="<?php echo site_url('/committees/unca') ?>">UNCA</a></li>
                                <li><a href="<?php echo site_url('/committees/undp') ?>">UNDP</a></li>
                                <li><a href="<?php echo site_url('/committees/unep') ?>">UNEP</a></li>
                                <li><a href="<?php echo site_url('/committees/unsc') ?>">UNSC</a></li>
																<li><a href="<?php echo site_url('/un4mun') ?>">UN4MUN</a></li>
                                <li><a href="<?php echo site_url('/committees/topic') ?>">Topic</a></li>
																<li><a href="<?php echo site_url('/committees/studyguide') ?>">Study guides</a></li>
					          </ul>
                            <li><a href="<?php echo site_url('/register') ?>">Register</a></li>
                            <li><a href="<?php echo site_url('/survey') ?>">Survey</a></li>
                            <li><a href="<?php echo site_url('/faq') ?>">FAQ</a></li>
					          <!--<li><a href="#">FAQ</a></li>-->
						</ul>
					</div>
				</div>
			</div>
			<!-- /.navbar-collapse -->
		</nav>
		<!-- /.container -->
	</header>
	<!-- .site-header-navbar-->
