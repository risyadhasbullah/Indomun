<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<link rel="stylesheet" href="<?php echo base_url(); ?>/css/slider.css">
	<!--  banner w3 end  -->

    <div id="IndonesiaMUN" class="container" style="padding-top:8em;">
        <div class="row">
            <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/oaml41uiI-Y' frameborder='0' allowfullscreen></iframe></div>
        </div>    
    </div>

	<section class="section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="banner-main text-center" style="color:black !important; padding: 0; padding-top: 3em;">
						<h3 class="lg-title wow fadeInLeft unselectable" >IndonesiaMUN<br>Pioneering Inclusivity Towards Sustainability</h3>
						<h5 class="sm-title wow fadeInLeft unselectable">9-13 November 2017</h5>
					</div>
                    
                    <div class="w3-content w3-display-container">
                      <img class="mySlides" src="<?php echo base_url(); ?>/img/events/panel_discussion.jpg" style="width:100%">
                      <img class="mySlides" src="<?php echo base_url(); ?>/img/events/beyond_mun.jpg" style="width:100%">

                      <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
                      <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
                    </div>

                    <script>
                    var slideIndex = 1;
                    showDivs(slideIndex);

                    function plusDivs(n) {
                      showDivs(slideIndex += n);
                    }

                    function showDivs(n) {
                      var i;
                      var x = document.getElementsByClassName("mySlides");
                      if (n > x.length) {slideIndex = 1}    
                      if (n < 1) {slideIndex = x.length}
                      for (i = 0; i < x.length; i++) {
                         x[i].style.display = "none";  
                      }
                      x[slideIndex-1].style.display = "block";  
                    }
                    </script>
                    
				</div>
			</div><!-- row end  -->
		</div><!-- container end  -->
	</section>
	
	
	<!-- section ABOUT Start	-->
	<section id="highlights" class="section-padding ">
		<div class="container">

			<div class="about-intro wow fadeInUp center" data-wow-delay='.2s'>
				<h2>Highlights of IndonesiaMUN 2017</h2>
			</div>

				<div class="row">
					<div class="col-md-6 wow fadeInUp center" data-wow-delay=".2s">
						<div class="about-img"> <img src="<?php echo base_url(); ?>/img/highlights/excellence.jpg" alt="IndonesiaMUN" class="img-responsive"> </div>
					</div>
					<div class="col-md-6 wow fadeInUp center hidden-sm hidden-xs" data-wow-delay=".4s">
						<div class="about-img"> <img src="<?php echo base_url(); ?>/img/highlights/UN4MUN.png" alt="IndonesiaMUN" class="img-responsive" style="margin-top: 5em;"> </div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6 px-5 center">
						<h3>Substance Excellence</h3>
					</div>
					<div class="col-md-6 px-5 center hidden-sm hidden-xs">
						<h3>UN4MUN</h3>
					</div>
				</div>

					<div class="row">
						<div class="col-md-6 px-5">
							<p>For years, IndonesiaMUN has proven to be the Model UN to provide delegates with the most relevant topics to date, presented in unique and challenging committees. Our Study Guides are composed carefully by our experienced directors from Universitas Indonesia's International delegations, and <b>for the first time ever, will enlist the assistance of relevant experts and governmental/ non-governmental entities in preparing our Study Guide. The relevant experts and agencies will also enlighten delegates directly in the conference room</b> this November. With that being said, we are very confident that the quality of substance are carefully monitored and perfectly suited to bring an authentic feel of a real UN conference.</p>
						</div>
						<div class="col-md-6 px-5 hidden-xs hidden-sm">
							<p>This year we are honored to introduce the <b>UN4MUN Procedure</b> to IndonesiaMUN 2017. Firstly introduced in 2013, UN4MUN is an initiative established by the <b>United Nations Department of Public Information</b> to showcase the most accurate UN Rules of Procedure to the world of Model UN. This new approach highlights the United Nations value of consensus to closely mirrors how the decision making process, negociation, and drafting in the United Nations actually work. As we will apply this procedure to one of our committee, this innovation will be the first of its kind both in Indonesia and in Southeast Asia. For more information about UN4MUN, you can visit https://outreach.un.org/mun/</p>
						</div>
					</div>

					<div class="row">
					<div class="col-md-6 wow fadeInUp center hidden-sm hidden-xs hidden-md hidden-lg" data-wow-delay=".2s">
						<div class="about-img"> <img src="<?php echo base_url(); ?>/img/highlights/excellence.jpg" alt="IndonesiaMUN" class="img-responsive"> </div>
					</div>
					<div class="col-md-6 wow fadeInUp center hidden-md hidden-lg" data-wow-delay=".4s">
						<div class="about-img"> <img src="<?php echo base_url(); ?>/img/highlights/UN4MUN.png" alt="IndonesiaMUN" class="img-responsive"> </div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6 px-5 center hidden-sm hidden-xs hidden-md hidden-lg">
						<h3>Substance Excellence</h3>
					</div>
					<div class="col-md-6 px-5 center hidden-md hidden-lg">
						<h3>UN4MUN</h3>
					</div>
				</div>

					<div class="row">
						<div class="col-md-6 px-5 hidden-sm hidden-xs hidden-md hidden-lg">
							<p>For years, IndonesiaMUN has proven to be the Model UN to provide delegates with the most relevant topics to date, presented in unique and challenging committees. Our Study Guides are composed carefully by our experienced directors from Universitas Indonesia's International delegations, and <b>for the first time ever, will enlist the assistance of relevant experts and governmental/ non-governmental entities in preparing our Study Guide. The relevant experts and agencies will also enlighten delegates directly in the conference room</b> this November. With that being said, we are very confident that the quality of substance are carefully monitored and perfectly suited to bring an authentic feel of a real UN conference.</p>
						</div>
						<div class="col-md-6 px-5 hidden-md hidden-lg">
							<p>This year we are honored to introduce the <b>UN4MUN Procedure</b> to IndonesiaMUN 2017. Firstly introduced in 2013, UN4MUN is an initiative established by the <b>United Nations Department of Public Information</b> to showcase the most accurate UN Rules of Procedure to the world of Model UN. This new approach highlights the United Nations value of consensus to closely mirrors how the decision making process, negociation, and drafting in the United Nations actually work. As we will apply this procedure to one of our committee, this innovation will be the first of its kind both in Indonesia and in Southeast Asia. For more information about UN4MUN, you can visit https://outreach.un.org/mun/</p>
						</div>
					</div>

				<div class="row">
					<div class="col-md-6 wow fadeInUp center" data-wow-delay=".2s">
						<div class="about-img"> <img src="<?php echo base_url(); ?>/img/highlights/experience.jpg" alt="IndonesiaMUN" class="img-responsive"> </div>
					</div>
					<div class="col-md-6 wow fadeInUp center hidden-sm hidden-xs" data-wow-delay=".4s">
						<div class="about-img"> <img src="<?php echo base_url(); ?>/img/highlights/prestigious.jpg" alt="IndonesiaMUN" class="img-responsive"> </div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6 px-5 center">
						<h3>Model UN Comprehensive Experience</h3>
					</div>
					<div class="col-md-6 px-5 center hidden-sm hidden-xs">
						<h3>The Oldest and The Most Prestigious MUN in Indonesia</h3>
					</div>
				</div>

					<div class="row">
						<div class="col-md-6 px-5">
							<p>IndonesiaMUN 2017 will offer a total comprehensive experience to our delegates both inside the conference room and outside. We believe that delegates shouldnot only excel in formal discussions, but to also forge new bonds of friendship and connections with others. Not only that, we believe that as youths we have to contribute directly to our society to start taking part in implementing solutions that we discuss in the conference rooms. Therefore we are providing unforgettable social events such as <b>Networking Night, Showcasing Indonesia’s Stakeholders,  a direct contributive campaign in the new ICareCampaign, BeyondMUN, Experts Workshop, and Diplomatic Dinner</b>. Also as a part of a full learning experience, we are inviting experts , diplomats, and official representatives of organizations to share their experience and to provide insight throughout the conference.</p>
						</div>
						<div class="col-md-6 px-5 hidden-sm hidden-xs">
							<p>IndonesiaMUN is the premier and the most prestigious Model UN in Indonesia. First established in 2010, we have consistently maintained our quality as one of the most competitive Model UN in the country; becoming the melting pot of the best and the brightest young minds of Indonesia and beyond. With seven years’ worth of participants ranging over a thousand of delegates, coming from domestic and overseas, this year will be our biggest and most ambitious session yet. IndonesiaMUN is organized by <b>Universitas Indonesia Model UN Club (UI MUN Club)</b>, led by its international delegations that have travelled to various Model UN in the world and have received awards from Harvard National Model UN (HNMUN), Harvard World Model UN (HWMUN), The World Federation of United Nations Associations (WFUNA) International Model UN (WIMUN), The European International Model UN (TEIMUN), The European Model UN (EUROMUN), The Asia Pacific Model UN Conference (AMUNC), SingaporeMUN, NTUMUN, and others.</p>
						</div>
					</div>

					<div class="row">
					<div class="col-md-6 wow fadeInUp center hidden-sm hidden-xs hidden-md hidden-lg" data-wow-delay=".2s">
						<div class="about-img"> <img src="<?php echo base_url(); ?>/img/highlights/experience.jpg" alt="IndonesiaMUN" class="img-responsive"> </div>
					</div>
					<div class="col-md-6 wow fadeInUp center hidden-md hidden-lg" data-wow-delay=".4s">
						<div class="about-img"> <img src="<?php echo base_url(); ?>/img/highlights/prestigious.jpg" alt="IndonesiaMUN" class="img-responsive"> </div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6 px-5 center hidden-sm hidden-xs hidden-md hidden-lg">
						<h3>Model UN Comprehensive Experience</h3>
					</div>
					<div class="col-md-6 px-5 center hidden-md hidden-lg">
						<h3>The Oldest and The Most Prestigious MUN in Indonesia</h3>
					</div>
				</div>

					<div class="row">
						<div class="col-md-6 px-5 hidden-sm hidden-xs hidden-md hidden-lg">
							<p>IndonesiaMUN 2017 will offer a total comprehensive experience to our delegates both inside the conference room and outside. We believe that delegates shouldnot only excel in formal discussions, but to also forge new bonds of friendship and connections with others. Not only that, we believe that as youths we have to contribute directly to our society to start taking part in implementing solutions that we discuss in the conference rooms. Therefore we are providing unforgettable social events such as Networking Night, a direct campaign in the new iCareCampaign, Gala Dinner, and excursion. Also as a part of a full learning experience, we are inviting experts , diplomats, and official representatives of organizations to share their experience and to provide insight throughout the conference.</p>
						</div>
						<div class="col-md-6 px-5 hidden-md hidden-lg">
							<p>IndonesiaMUN is the premier and the most prestigious Model UN in Indonesia. First established in 2010, we have consistently maintained our quality as one of the most competitive Model UN in the country; becoming the melting pot of the best and the brightest young minds of Indonesia and beyond. With seven years’ worth of participants ranging over a thousand of delegates, coming from domestic and overseas, this year will be our biggest and most ambitious session yet. IndonesiaMUN is organized by <b>Universitas Indonesia Model UN Club (UI MUN Club)</b>, led by its international delegations that have travelled to various Model UN in the world and have received awards from Harvard National Model UN (HNMUN), Harvard World Model UN (HWMUN), The World Federation of United Nations Associations (WFUNA) International Model UN (WIMUN), The European International Model UN (TEIMUN), The European Model UN (EUROMUN), The Asia Pacific Model UN Conference (AMUNC), SingaporeMUN, NTUMUN, and others.</p>
						</div>
					</div>

					

		</div><!-- container end  -->
	</section>
	<!-- section ABOUT end	-->

	<!--  Letter start  -->
	<section id="letter" class="section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-offset-2 download-wrap wow fadeInUp">
						<h2>Letter from the Secretary General</h2>
						<b>
						<img class="pull-left marg-pic" src="<?php echo base_url(); ?>/img/SG.png" alt="IndonesiaMUN">
						<p>It is my pleasure to welcome you to the Official Website of the Eighth Session of Indonesia Model United Nations! My name is Kenneth Nicholas and I am honored to be serving as your Secretary-General for IndonesiaMUN 2017. </p>
						<p>Since 2010, we have provided the most comprehensive platform for youths in Indonesia and other nations in empowering the next generation to voice their concerns and actively engage and contribute to our world. In doing so, we have always innovated and bring new ideas to give the best experience of Model UN to our delegates. We firmly believe that IndonesiaMUN is not only a mere competition, but it is one of our effort to promote United Nations values and programs, and to raise awareness of the international issues the world still faces. Poverty, war, inequality, environmental degradation, and human rights violations are some of the examples of issues that require the attention of every stakeholder including the young generation. Therefore, in IndonesiaMUN, we encourage every delegate to not only discuss and provide solutions in the conference, but also try to tackle these problems outside of the conference. </p>
						<p>This year, we picked up PIONEERING INCLUSIVITY TOWARDS SUSTAINABILITY as our theme, inspired by Goal 17 of the Sustainable Development Goals Agenda 2030: “<i>Strengthening the means of implementation and revitalize the global partnership for sustainable development.</i>” We want to disseminate the value of Inclusivity of the United Nations to you, global citizens and to invite you to take part in building the world together with Governments, Civil Societies, the Private Sector, and the United Nations. This inclusive attitude will gain the full participation of all relevant stakeholders and the result is a multi-dimensional approach in policies that works for everyone. When everyone contributes their fair share in this global partnership, implementation of solutions is in the best interest for everyone involved.</p>
						<p>We believe that IndonesiaMUN 2017 will serve as a room for delegates to connect with very important people and build broader networks for the future. In doing so, we are continuing the tradition of innovating by introducing new ways of preparing the best substance, social side-events, and other areas to give you the best experience this November. So, stay tune to our website, and other means of publication as we reveal our plans for IndonesiaMUN 2017 in the upcoming months.</p>
						<p>Sincerly,</p>
						<img src="<?php echo base_url(); ?>/img/Signature.png" alt="IndonesiaMUN">
						<p>Kenneth Nicholas<br>
						Secretary General of IndonesiaMUN 2017</p>
						</b>
					</div>
				</div>
			</div>
		</div><!-- container end  -->
	</section>