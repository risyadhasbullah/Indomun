<style>
#main-banner {
	background: url("<?php echo base_url(); ?>/img/council/aleppo.jpg") fixed 50% 50%;
    background-repeat: no-repeat;
}
</style>
<!-- section unsc Start	-->
<section id="unsc" class="section-padding" style="padding-bottom: 0;">
    <div class="container">
        <div class="col-md-5 col-sm-12 hidden-sm wow fadeInUp" data-wow-delay='.4s'>
            <div class="about-intro">
                <div class="row">
                    <h2 class="center">UNSC Council</h2>
                    <p>The United Nation Security Council (UNSC) has the function to protect international peace and security. Their aim is to preserve the the regional peace in every state and to uphold peace between each other states. It is responsible to formulate plans and submit them then to United Nations members, for the establishment of a system regulating armaments. UNSC has a lawfully binding resolution and able to implement real penalty such as embargo and intervention. It is the only council where veto power applies.</p>

                    <p>Meet the director of UNSC council, Adfikri Kevin Marvel of Faculty of Social and Political Sciences 2014. His experience has been proven by being the official delegation for Asia Pacific MUN 2016, winning the Most Outstanding Delegate in IRDUMUN 2015 Council UNSC, and being the assistant director for IMUN 2016 Council UNICEF.</p>

                    <p>In the council of UNSC, we have more than one director. Kevin is accompanied by Regina Cara Riantoputra from Faculty of Economics 2014 who is the top ranked 10% Best Debate Speaker Worldwide (#68/756) . She is the Top Indonesian Speaker in World School Debating Championship 2014 and the Highest Ranking Indonesian Team in World University Debating Championship since 1981 beating Yale, Harvard, and Cornell. Recently she represented UI in Harvard National Model UN 2017. Besides Cara, the Security council will also be chaired by I Gede Sthitaprajana Virananda from Faculty of Economics 2016 who is the winner of the Most Outstanding Delegate on ALSA E-MUN 2016 and the Best Novice Speaker of IVED 2017. He is currently part of UI delegation to Asia Pacific MUN Conference 2017 (AMUNC). </p>
                </div>
            </div>
        </div><!-- col-6  -->
        <div class="col-md-7 four-images">
            <div class="row wow fadeInDown" data-wow-delay='.4s'>
                <img class="fix-margin" src="<?php echo base_url(); ?>/img/council/unsc-logo.jpg" alt="unsc" style="height:15em;">
                <img src="<?php echo base_url(); ?>/img/council/unsc1.jpg" alt="unsc" style="height:15em;">
            </div>
            <div class="row wow fadeInUp" data-wow-delay='.4s'>
                <img class="fix-margin" src="<?php echo base_url(); ?>/img/council/unsc2.jpg" alt="unsc" style="height:15em;">
                <img src="<?php echo base_url(); ?>/img/council/unsc3.jpg" alt="unsc" style="height:15em;">
            </div>
        </div>
    </div>
    <hr>
</section>