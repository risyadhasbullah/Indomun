<style>#loader{margin-top: -2em;}</style>
<section class="section-padding" style="margin-top:3em; padding-bottom:0;">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <img src="<?php echo base_url(); ?>/img/generalassembly.jpg" width="100%" alt="General Assembly">
        <p>Photo : UN GA Plenary Hall</p>
      </div>
    </div><!-- row end  -->
  </div><!-- container end  -->
</section>
<section class="section-padding">
    <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="banner-main text-center" style="color:black !important; padding: 0;">
                  <img src="<?php echo base_url(); ?>/img/highlights/UN4MUN.png" alt="UN4MUN">
                  <h4 class="lg-title unselectable">Description of UN4MUN</h4>
              </div>
                <p class="unselectable">UN4MUN is the most recent and cutting-edge Model UN platform (set of MUN Rules of Procedure), that was developed by the United Nations Department for Public Information to provide an accurate simulation of the United Nations, and a thorough understanding of its decision-making process. It follows authentic UNGA, and UNSC Procedure, which in turn makes UN4MUN conferences very different from the Traditional Model UN that we have come to know over the years. The key differences of traditional Model UN and the UN4MUN procedure primarily revolves around The Leadership Structure, Rules of Procedure, The Negotiation Process and the Nomenclatures that are used.<br><br>

                It was developed and is currently managed by the Model UN office at the Education Outreach Section, Outreach Division of the UNDPI at the UN HQ in New York.
                Their website: www.outreach.un.org/mun. <br><br>

                For the first time in Indonesia, IndonesiaMUN will host the first UN4MUN Committee, with the topic “Coordinated Effort to Combat Trafficking in Persons”. This committee is created in association with the Indonesian Student Association for International Affairs (ISAFIS); particularly Sylvania Jeshuani (Secretary General of UN4MUN Southeast Asia Conference, Thailand 2018), Petrus Raditya (Diplomacy Award of WFUNA International Model UN India 2016), and Omar Soemadipradja (Vice-Chair of the WFUNA International Model UN 2017, New York).<br><br>

                IndonesiaMUN represented by our Secretary General, Kenneth Nicholas was one of the speaker at the “UN4MUN & You : Introduction to the Accurate UN Simulation” event by ISAFIS on 22 August 2017 held in @America, Pacific Place, Jakarta with Sylvania Jeshuani and Omar Soemadipradja. The event marks the first public introduction to UN4MUN, and we are very excited to finally give you the chance to try it this November ! William (Bill) Yotive, the Co-Developer of the UN4MUN Procedure and now MUN Coordinator at WFUNA was also part of the event via Teleconference from New York, USA.
                  <br><br></p>

                <p>Sources:<br>
                  https://www.unitedambassadors.com/un4mun-introduction<br>
                  https://outreach.un.org/mun/#</p>

                <div class="text-center">
                <a href="https://www.unitedambassadors.com/#!What-is-UN4MUN-The-Top-10-Questions-Youll-Need-To-Get-Started/ju7bo/572863620cf2877f1bef140a">Visit the UN4MUN FAQ's <u>here</u> if you have any questions.</a>
                </div>
          </div>
        </div>

        <div class="row">
          <div class="banner-main text-center" style="color:black !important; padding: 0;">
            <h4 class="lg-title unselectable" >ISAFIS UN4MUN Event in @America</h4>
          </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
              <img src="<?php echo base_url(); ?>/img/un4mun/atamerica-1.jpg" width="100%" alt="@america">
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
              <img src="<?php echo base_url(); ?>/img/un4mun/atamerica-2.jpg" width="100%" alt="@america">
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
              <img src="<?php echo base_url(); ?>/img/un4mun/atamerica-3.jpg" width="100%" alt="@america">
            </div>
        </div>

        <div class="row" style="padding-top: 2em;">
          <div class="col-md-4 col-sm-12 col-xs-12 text-center">
            <img src="<?php echo base_url(); ?>/img/un4mun/atamerica-4.jpg" width="100%" alt="@america">
          </div>
          <div class="col-md-4 col-sm-12 col-xs-12 text-center">
            <img src="<?php echo base_url(); ?>/img/un4mun/atamerica-6.jpg" width="100%" alt="@america">
          </div>
          <div class="col-md-4 col-sm-12 col-xs-12 text-center">
            <img src="<?php echo base_url(); ?>/img/un4mun/atamerica-5.jpg" width="100%" alt="@america">
          </div>
        </div>
        <hr style="border-top:2.3px solid #e5e5e5;">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="banner-main text-center" style="color:black !important; padding: 0;">
                  <h4 class="lg-title unselectable">Bureau of Secretariat</h4>
              </div>
                <p class="unselectable">In UN4MUN, we have the Bureau of Secretariat that will guide the conference. The Bureau consists of a Secretary, a Chair, and a Rapporteur. The Secretary is the representative of the Model UN Secretariat and will act as a passive facilitator to monitor delegates on the matters of Rules and Procedure and the substance. On the other hand, the Chair will be an active moderator that will guide the debate in the committee using the GA Committee Chairing Guide, supervised by the Secretary. What’s different between the UN4MUN Procedure and the Traditional Model UN is that the Chair will be elected from amongst the delegates and will chair and represent his/her country at the same time. This method is based on the real UN proceeding in the General Assembly. Lastly, The Rapporteur will document the sessions in writing and help the Secretary and Chair keep track of the debate. <br><br>

                Our Secretary<br>
                Omar Soemadipradja<br></p>
                <div class="banner-main text-center" style="color:black !important; padding: 0;">
                  <img src="<?php echo base_url(); ?>/img/un4mun/omar.jpg" width="50%" alt="Omar Soemadipradja">
                </div>

                <p><br/>Omar was formerly the Vice-Chair of the WFUNA International Model UN 2017, held in UN Headquarters, New York, specifically General Assembly Committee3. As the Vice-Chair, he was personally trained by William (Bill) Yotive for the UN4MUN Procedure himself.  He is now serving as the Under Secretary General for the UN4MUN South East Asia Conference (USEAC) 2018 , the first full UN4MUN Conference of Southeast Asia, held in Bangkok, Thailand.  Recently, he appeared as a guest speaker on a talk show organized by the Indonesian Student Association For International Studies (ISAFIS) titled “UN4MUN and You” in Jakarta, where he shared his experience with the UN4MUN procedure to Indonesian students.   He is now a final student undertaking Law in Atma Jaya University.</p><br><br>
          </div>
        </div>
    </div>
</section>
