<style>
#main-banner {
	background: url("<?php echo base_url(); ?>/img/council/prison.jpg") fixed 50% 50%; 
    background-repeat: no-repeat;
}
</style>
<!-- section sochum Start	-->
<section id="sochum" class="section-padding" style="padding-bottom: 0;">
    <div class="parallax" style="background-image: url('<?php echo base_url(); ?>/img/slider/bg-1.jpg');">

    </div>
    <div class="container">
        <div class="col-md-6 col-sm-12 wow fadeInUp" data-wow-delay='.4s'>
            <div class="about-intro">
                <div class="row">		
                    <h2 class="center">SOCHUM Council</h2>
                    <p>United Nation’s third Committee of the General Assembly is known as SOCHUM (Social Cultural and Humanitarian Committee). SOCHUM discusses and offers solutions for social, cultural, and humanitarian matters. Over time, the work has evolved to deal with mostly human rights issues, making it the world’s largest and most prominent forum for international human rights norm creation. They are tasked with a broad mandate from around the world including human rights, global literacy, women’s & children’s rights, the treatment of refugees and displaced persons, international drug control, crime prevention, and the elimination of racism and discrimination.</p>

                    <p>Meet our Director for SOCHUM, Shinta Wulandari Basuki! Despite her only being a second year student, she has won oustanding award such as Best Delegates of BirFestMUN 2015. She has also become one of UI Official Delegation for Harvard World MUN 2016 and Asia Pasific MUN Conference 2017. Furthermore, she was the Director of SOCHUM for GrandGeneralAssembly 2016.</p>

                    <p>In this council, Shinta Wulandari is also helped by two brilliant assistant directors: Christella Feni from Faculty of Social and Political Sciences 2016 and Kaleb from Faculty of Law 2016. Christella Feni is the winner of this year's Diplomacy Award on WorldMUN 2017 and also the best delegate for JakartaMUN and IndonesiaMUN 2016, while Kaleb is the Indonesian delegation of EuroMUN and verbal commendation of IMUN 2016.</p>
                </div>
            </div>
        </div><!-- col-6  -->
        <div class="col-md-6">
            <div class="row wow fadeInUp" data-wow-delay='.4s'>
                <img class="center-image center" src="<?php echo base_url(); ?>/img/council/sochum-logo.jpg" alt="sochum" style="height:15em;">
            </div>
            <div class="row wow fadeInUp" data-wow-delay='.8s'>
                <img class="center-image center" src="<?php echo base_url(); ?>/img/council/sochum1.jpg" alt="sochum" style="height:15em;">
            </div>
            <div class="row wow fadeInUp" data-wow-delay='1.2s'>
                <img class="center-image center" src="<?php echo base_url(); ?>/img/council/sochum2.jpg" alt="sochum" style="height:15em;">
            </div>
        </div>
    </div>
    <hr>
</section>