<section class="section-padding" style="margin-top:3em; padding-bottom:0;">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <a href="/preevents"><img src="<?php echo base_url(); ?>/img/openmun101.jpg" width="100%" alt="IMUN 101 Poster"></a>
      </div>
    </div><!-- row end  -->
  </div><!-- container end  -->
</section>
<section class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="banner-main text-center" style="color:black !important; padding: 0;">
                    <h3 class="lg-title wow fadeInLeft unselectable" >Indonesia MUN 101 Workshop</h3>
                      <p class="wow fadeInLeft unselectable">Greeting Future Delegates! Indonesia MUN 101 is a workshop and talkshow event held annually by UI MUN Club which aims to assist three essential skills in Model United Nations; Speech, Drafting and Negotiation. Indonesia MUN 101 is the appropriate event for you to learn and master the procedures behind Model United Nations and the skills that it takes to be the best delegate!<br><br>
                      <a href="http://tiny.cc/imun101regist">Click here</a> for registration!</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="banner-main text-center" style="color:black !important; padding: 0; padding-top: 3em;">
                    <h3 class="lg-title wow fadeInLeft unselectable" >Indonesia MUN 101 Poster Competition</h3>
                      <p class="wow fadeInLeft unselectable">
                      Indonesia MUN 101 poster competition is a competition aimed to increase the awareness of youths towards current social and political issues through a creative medium. The grand theme for the poster competition is "“No One Left Behind: The Importance of Inclusivity in Solving Global Issues” followed by a subtopic that the participants must choose. Participants must choose one out of the five existing topics discussed in IndonesiaMUN 2017.<br><br>
                      <a href="http://tiny.cc/imun101postercomp">Click here</a> for registration!</p>
                </div>
            </div>
        </div>
    </div>
</section>
